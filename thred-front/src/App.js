import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import ThreadBuilder from "./containers/ThreadBuilder/ThreadBuilder";

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={ThreadBuilder}/>
            </Switch>
        </Layout>
    );
}

export default App;
