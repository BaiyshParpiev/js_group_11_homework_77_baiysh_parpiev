import {
    FETCH_THREADS_FAILURE,
    FETCH_THREADS_REQUEST,
    FETCH_THREADS_SUCCESS, MODAL_CLOSE,
    MODAL_OPEN
} from "../actions/threadActions";

const initialState = {
    fetchLoading: false,
    threads: [],
    modal: false,
};

const threadReducer = (state = initialState, action) => {
    switch(action.type){
        case MODAL_OPEN:
            return {...state, modal: true};
        case MODAL_CLOSE:
            return {...state, modal: false};
        case FETCH_THREADS_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_THREADS_SUCCESS:
            return {...state, threads: action.payload, fetchLoading: false};
        case FETCH_THREADS_FAILURE:
            return {...state, fetchLoading: false};
        default:
            return state;
    }
};

export default threadReducer;