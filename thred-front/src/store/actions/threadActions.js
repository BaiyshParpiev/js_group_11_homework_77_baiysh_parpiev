import axios from "axios";

export const FETCH_THREADS_REQUEST = 'FETCH_THREADS_REQUEST';
export const FETCH_THREADS_SUCCESS = 'FETCH_THREADS_SUCCESS';
export const FETCH_THREADS_FAILURE = 'FETCH_THREADS_FAILURE';


export const CREATE_THREAD_REQUEST = 'CREATE_THREAD_REQUEST';
export const CREATE_THREAD_SUCCESS = 'CREATE_THREAD_SUCCESS';
export const CREATE_THREAD_FAILURE = 'CREATE_THREAD_FAILURE';
export const MODAL_OPEN = 'MODAL_OPEN';
export const MODAL_CLOSE = 'MODAL_CLOSE';

export const modalOpen = () => ({type: MODAL_OPEN});
export const modalClose = () => ({type: MODAL_CLOSE});

export const fetchThreadsRequest = () => ({type: FETCH_THREADS_REQUEST});
export const fetchThreadsSuccess = threads => ({type: FETCH_THREADS_SUCCESS, payload: threads});
export const fetchThreadsFailure = () => ({type: FETCH_THREADS_FAILURE});


export const createThreadRequest = () => ({type: CREATE_THREAD_REQUEST});
export const createThreadSuccess = () => ({type: CREATE_THREAD_SUCCESS});
export const createThreadFailure = () => ({type: CREATE_THREAD_FAILURE});

export const fetchThreads = () => {
    return async dispatch => {
       try{
           dispatch(fetchThreadsRequest());
           const response = await axios.get('http://localhost:8000/threads');
           dispatch(fetchThreadsSuccess(response.data))
       }catch(e){
           dispatch(fetchThreadsFailure());
       }
    }
};



export const createThread = threadsData => {
    return async dispatch => {
       try{
           dispatch(createThreadRequest());
           await axios.post('http://localhost:8000/threads/', threadsData);
           dispatch(createThreadSuccess())
       }catch(e){
           dispatch(createThreadFailure());
           throw e;
       }
    }
};