import {createTheme} from '@material-ui/core';

const defaultTheme = createTheme();

const theme = createTheme({
    props: {
        MuiTextField: {
            variant: 'outlined',
            fullWidth: true,
        }
    }
});

export default theme;