import React from 'react';
import {makeStyles, Modal, Button, Paper} from '@material-ui/core';
import NewThread from "../../../containers/NewThread/NewThread";
import {useDispatch, useSelector} from "react-redux";
import {modalClose, modalOpen} from "../../../store/actions/threadActions";


const useStyles = makeStyles((theme) => ({
    paper: {
        position: 'absolute',
        width: 400,
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)'
    },
}));

const SimpleModal = ({history}) =>  {
    const classes = useStyles();
    const dispatch = useDispatch();
    const modal = useSelector(state => state.modal);

    const handleOpen = () => {
        dispatch(modalOpen());
    };

    const handleClose = () => {
        dispatch(modalClose())
    };


    return (
        <div>
            <Button onClick={handleOpen} type="submit" color="primary" variant="contained">Create new thread</Button>
            <Modal
                open={modal}
                onClose={handleClose}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                <Paper className={classes.paper}>
                    <NewThread history={history}/>
                </Paper>
            </Modal>
        </div>
    );
}

export default SimpleModal