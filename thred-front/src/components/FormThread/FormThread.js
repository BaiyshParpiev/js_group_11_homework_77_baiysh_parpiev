import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import FileInput from "../UI/FileInput/FileInput";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
}));

const ProductForm = ({onSubmit}) => {
    const classes = useStyles();

    const [state, setState] = useState({
        author: "",
        message: "",
        image: null,
    });

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prev => {
            return {...prev, [name]: file}
        });
    }

    return (
        <Grid
            container
            direction="column"
            spacing={2}
            component='form'
            className={classes.root}
            autoComplete='off'
            onSubmit={submitFormHandler}>
            <Grid item xs>
                <TextField
                    label="Author"
                    value={state.author}
                    onChange={inputChangeHandler}
                    name="author"
                />
            </Grid>
            <Grid item xs>
                <TextField
                    required
                    label="Message"
                    value={state.message}
                    onChange={inputChangeHandler}
                    name="message"
                />
            </Grid>
            <Grid item xs>
                <FileInput
                    name="image"
                    label='Image'
                    onChange={fileChangeHandler}
                />
            </Grid>
            <Grid item container xs justifyContent='center'>
                <Button type="submit" color="primary" variant="contained">Create</Button>
            </Grid>
        </Grid>
    );
};

export default ProductForm;
