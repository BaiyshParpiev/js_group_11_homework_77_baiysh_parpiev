import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, compose, createStore} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from "react-redux";
import {ThemeProvider} from '@material-ui/core';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import theme from "./theme";
import threadReducer from "./store/reducers/threadReducer";
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const store = createStore(threadReducer, composeEnhancers(applyMiddleware(thunk)));

const app = (
    <Provider store={store}>
        <ThemeProvider theme={theme}>
            <BrowserRouter>
                <App/>
            </BrowserRouter>
        </ThemeProvider>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));

