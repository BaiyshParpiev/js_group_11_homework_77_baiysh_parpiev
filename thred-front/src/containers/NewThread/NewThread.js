import {Typography} from "@material-ui/core";
import {useDispatch} from "react-redux";
import FormThread from "../../components/FormThread/FormThread";
import {createThread, modalClose} from "../../store/actions/threadActions";

const NewProduct = ({history}) => {
    const dispatch = useDispatch();

    const createProductHandler = async threadData => {
        await dispatch(createThread(threadData));
        dispatch(modalClose());
        history.replace('/')
    }

    return (
        <>
            <Typography variant="h4">New Thread</Typography>
            <FormThread onSubmit={createProductHandler}/>
        </>
    );
};

export default NewProduct;