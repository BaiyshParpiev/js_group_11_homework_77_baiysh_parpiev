import React, {useEffect} from 'react';
import SimpleModal from "../../components/UI/Modal/Modal";
import {Grid, Typography} from "@material-ui/core";
import Thread from "../Thread/Thread";
import {useDispatch, useSelector} from "react-redux";
import {fetchThreads} from "../../store/actions/threadActions";

const ThreadBuilder = ({history}) => {
    const dispatch = useDispatch();
    const threads = useSelector(state => state.threads);
    const modal = useSelector(state => state.modal)

    useEffect(() => {
        dispatch(fetchThreads());
    }, [dispatch, modal]);


    return (
        <Grid container direction='column' spacing={2}>
            <Grid item container justifyContent='space-between' alignItems='center'>
                <Grid item>
                    <Typography variant='h4'>Last posts</Typography>
                </Grid>
                <Grid item>
                    <SimpleModal history={history}/>
                </Grid>
            </Grid>
            <Grid item container direction="row" spacing={2}>
                {threads && threads.map(thread => (
                    <Thread
                        key={thread.id}
                        id={thread.id}
                        author={thread.author}
                        message={thread.message}
                        image={thread.image}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default ThreadBuilder;