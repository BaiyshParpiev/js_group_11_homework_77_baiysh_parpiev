import React from 'react';
import {CardMedia, makeStyles, Typography, Card, Grid, CardContent} from "@material-ui/core";
import {apiURL} from "../../config";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%' //16.9
    }
});

const ProductItem = ({author, message, image}) => {
    const classes = useStyles();
    let cardAuthor = 'Anonymous'

    if(author){
        cardAuthor = author;
    }

    let cardImage;

    if(image){
        cardImage = apiURL + '/uploads/' + image;

    }

    return ( <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card classame={classes.card}>
                {image && <>
                    <CardMedia
                        image={cardImage}
                        title={author}
                        className={classes.media}
                    />
                </>}
                <CardContent>
                    <Typography varian='subtitle1'>
                        {cardAuthor}
                    </Typography>
                    <Typography varian='body1'>
                        {message}
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
    );
};

export default ProductItem;