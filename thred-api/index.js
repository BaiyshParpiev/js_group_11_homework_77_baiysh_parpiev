const express = require('express');
const threads = require('./app/threads');
const cors = require('cors');
const fileDb = require('./fileDb');

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));



const port = 8000;

app.use('/threads', threads);

fileDb.init();
app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});
