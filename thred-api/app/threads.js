const express = require('express');
const router = express.Router();
const fileDb = require('../fileDb');
const path = require("path");
const multer = require("multer");
const {nanoid} = require("nanoid");
const config = require("../config");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});


const upload = multer({storage});



router.get('/', (req, res) => {
  const thread = fileDb.getItems();
  res.send(thread);
});

router.post('/', upload.single('image'), (req, res) => {
  if(!req.body.message){
    return res.status(404).send({error: 'Date not valid'});
  }

  const thread = {
    author: req.body.author,
    message: req.body.message,
  }

  if(req.file){
    thread.image = req.file.filename;
  }

  const newProduct = fileDb.addItem(thread)
  res.send(newProduct);
});

module.exports = router
